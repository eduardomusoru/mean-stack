const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const postsRoutes = require('./routes/posts');
const userRoutes = require('./routes/user');

// return an express app
const app = express();

const mongooseConfig = {
  useNewUrlParser: true,
  useUnifiedTopology: true
}; 
mongoose.connect('mongodb+srv://edd:UkZ89br4Lst8rnMu@cluster0-9oge1.mongodb.net/test?retryWrites=true&w=majority', mongooseConfig)
  .then(() => {
    console.log('Connected to DB');
  }).catch(() => {
    console.log('Connection to the DB failed');  
  }) 
; 
  
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/images', express.static(path.join('backend/images'))); // fw images to backend/images

app.use((req, res, next) => {  
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Request-Width, Content-Type, Accept, Authorization'
  );
  res.setHeader('Access-Control-Allow-Methods', 
  'GET, POST, PATCH, DELETE, OPTIONS, PUT');

  next();
});
app.use('/api/posts', postsRoutes);
app.use('/api/user', userRoutes);

// register what we want to export
module.exports = app;
